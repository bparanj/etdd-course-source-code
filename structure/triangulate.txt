Example 1 : Adding Numbers

Here's an example, Suppose we want to write a function that will return the sum of two integers, We write:

test_addition
assert_equal 4, calculator.add(1, 3)

def addition
  return 4
  
If we are triangulating to the right design, we have to write:

assert_equal 7, add(3,4)

When we have the second example, we can abstract the implementation of add()

add(augend, addend)
  return augend + addend
  
Once we have the two assertions and we have abstracted the correction implementation for add, we can delete one of the assertions because it is redundant. 

Example 2 : Summing a List of Numbers

Suppose we want to write a logic that creates an aggregate sum of the list. Let’s assume that we have no idea how to design the internals of our custom list class so that it fulfills its responsibility. Thus, we start with the simplest example of calculating a sum of 0 elements:

result = sum(elements)
assert.equal(0, result)

sum
  return 0

This is not yet the implementation we are happy with, which makes us add another test, this time for a single element.

The naive implementation can be as follows:

sum
  return element

We have two examples, so let’s check whether we can generalize now. We could simplify our implementation, let’s wait just a little bit longer and see if this is the right path to go.

Let’s add third example then. What would be the next more complex one? Note that the choice of next example is not random. Triangulation is about considering the axes of variability. If you carefully read the last example, you probably noticed that we already skipped one axis of variability - the value of the element. We used any.integer where we could use a literal value and add a second example with another value to make us turn it into variable. This time, however, I decided to type the obvious implementation. The second axis of variability is the number of elements. The third example will move us further along this axis - so it will use two elements instead of one or zero. This is how it looks like:

assert.equal(first element + second element, result)

And the naive implementation will look like this:

sum
return element1 + element 2

After adding and implementing the third example, the variability of elements count becomes obvious. Now that we have three examples, we see even more clearly that we have redundant constructors and redundant fields for each element in the list and if we added a fourth example for three elements, we’d have to add another constructor, another field and another element of the sum computation. Time to generalize!

How do we encapsulate the variability of the element count so that we can get rid of this redundancy? A collection! How do we generalize the addition of multiple elements? A foreach loop through the collection! 

While the first test (“no elements") seems like a special case, the remaining two -- for one and two elements -- seem to be just two variations of the same behavior (“some elements"). Thus, it is a good idea to make a more general test that describes this logic to replace the two examples. After all, we don’t want more than one failure for the same reason. So as the next step, I will write a test to replace these examples (I leave them in though, until I get this one to evaluate to true).

This test uses three values rather than zero, one or two as in the examples we had. I pick 3, which is the smallest number that has distinct head, tail and middle element. One or two elements seem like a special case, while three sounds generic enough.

One more thing we can do is to ensure that we didn’t write a false positive, i.e. a test that is always true due to being badly written. In other words, we need to ensure that the test we just wrote will ever evaluate to false if the implementation is wrong. As we wrote it after the implementation is in place, we do not have this certainty.

What we will do is to modify the implementation slightly to make it return wrong value and see how our test will react (we expect it to evaluate to false):

When we do this, we can see our last test evaluate to false with a message like “expected 21, got 22". We can now undo this one little change and go back to correct implementation.

The examples (“zero elements", “one element" and “two elements") still evaluate to true, but it’s now safe to remove the last two, leaving only the test about a behavior we expect when we calculate sum of no elements and the test about N elements we just wrote.

We have arrived at the final, generic solution. Note that the steps we took were tiny -- so you might get the impression that the effort was not worth it. Triangulation shows its power in more complex problems with multiple design axes and where taking tiny steps helps avoid “analysis paralysis".

 Triangulation is most useful when you have no idea how the internal design of a piece of functionality will look like and it’s not obvious along which axes your design must provide generality, but you are able to give some examples of the observable behavior of that functionality given certain inputs. These are usually situations where you need to slow down and take tiny steps that slowly bring you closer to the right design and functionality -- and that’s a good fit for triangulation.




Triangulate
===============

How do you most convervatively drive abstraction with tests? Only abstract when you have two or more examples.

This is the third and most conservative implementation strategy.

add(augend, addend)
augend + addend

Triangulation is attractive because the rules for it seem so clear. The rules for Fake It, where we are relying on our sense of duplication between the test case and the fake implementation to drive abstraction, seem a bit vague and subject to interpretation.

I only use triangulation when I'm unsure about the correct abstraction for the calculation. Otherwise, I relay on either Obvious Implementation or Fake It.

CREATE A NEW FILE AND COMBINE WITH GOOGLE DOCS.


Define triangulation in Math. High level demo.

When we triangulate, we only generalize code when we have 2 or more examples. We briefly ignore the duplication between the test and model code. When the second example demands a more general solution, then and only then do we generalize.

Ex: 

$5 x 2 = $ 10
$5 x 3 = $ 15

We can no longer return a constant.   
Test Data

What data do you use for test-first tests? Use data that makes the tests easy to read and follow.

If there is a difference in the data, it should be meaningful. If there isn't a conceptual difference between 1 and 2, use 1. 

Test Data isn't a license to stop short of full confidence. If your system has to handle multiple inputs, your tests should reflect multiple inputs. However, don't have a list of 10 items as the input data if a list of 3 items will lead you to the same design and implementation decisions.

One trick in Test Data is to try to never use the same constant to mean more than one thing. For add method, using 1, 1 or 2, 2 is not a good idea. What if we got the arguments reversed in the implementation? In this case, it does not affect. But it must be considered.

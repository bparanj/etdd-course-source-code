Test List

When we sit down to a programming session, what is it we intend to accomplish?

I got in the habit of writing down everything I wanted to accomplish over the next few hours on a slip of paper next to my computer. I had a similar list, but with weekly or monthly scope pinned on the wall. As soon as I had all that written down, I knew I wasn't going to forget something. When a new item came up, I would quickly and consciously decide whether it belonged on the 'now' list, the 'later' list or it didn't really need to be done at all.

Applied to TDD, what we put on the list are the tests we want to implement. First, put on the list examples of every operation that you know you need to implement. Next, for those operations that don't already exist, put the null version of that operation on the list. Finally, list all the refactorings that you think you will have to do to have clean code at the end of this session.

Writing tests en masse hasn't worked for me. First, every test you implement is a bit of inertia when you have to refactor. When you've implemented 10 tests and then you discover the arguments need to be in the opposite order, you are less likely to go clean up. Second, if you have 10 tests broken, you are a long way from the green bar. If you want to get to green quickly, you have to throw all 10 tests away. If you want to get all the tests working, you are going to be staring at red bar for a long time. If you are sufficiently addicted to the green bar, you cannot be in red-state for a long time. 

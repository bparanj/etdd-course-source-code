How does TDD relate to patterns?

My writings are about finding fundamental rules that generate behavior similar to experts. By reducing repeatable behavior to rules, applying the rules becomes rote and mechanical. This is quicker than re-debating everything from first principles all the time. When there is an exception or a problem that just doesn't fit any of the rules, you have more time and energy to generate and apply creativity.

When should you delete tests?
===============

More tests is better, but if two tests are redundant with respect to each other, should you keep them both around?
The first criterion for your tests is confidence. Never delete a test if it reduces your confidence in the behavior of the system.

The second criterion is communication. If you have two tests that exercise the same path through the code, but they speak to different scenarios for a readers, leave them alone.

That said, if you have two tests that are redundant with respect to confidence and communication, delete the least useful of the two.

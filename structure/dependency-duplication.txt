Dependency and Duplication

The problem with the test and code is the dependency between the code and the test. You can't change one without changing the other. Our goal is to be able to write another test that 'makes sense' to use, without having to change the code, something that is not possible with the current implementation.

Dependency is the key problem in software development at all scales.

If dependency is the problem, duplication is the symptom. Duplication most often takes the form of duplicate logic. Objects can abstract away the duplication of logic. Eliminating duplication eliminates dependency. That's why the second rule appears in TDD. By eliminating duplication before we go on to the next test, we maximize our chance of being able to get the next test running with one and only one change. 

Usually you see duplication between two pieces of code. Here the duplication is between the data in the test and the data in the code.

The test still passes. Do these steps seem too small to you? Remember, TDD is not about taking teensy tiny steps. When things get the least bit weird, I am glad I can. If you can make steps too small, you can certainly make steps the right size. If you only take larger steps, you'll never know if smaller steps are appropriate.

THIS ANSWERS THE QUESTION : WHY SECOND RULE?



We don't have code duplication, but we do have data duplication. Before when we've had a fake implementation, it's been obvious how to work backwards to the real implementation. We replace constants with variables. This time, it's not obvious to me how to work backwards. So, even though it feels a little speculative, we'll work forwards. 

The test above is not one I would expect to live a long time. It is deeply concerned with the implementation of our operation, not its externally visible behavior. However, if we make it work, we expect we've moved one step closer to our goal. Ugly solution. We are green and refactoring is possible.

- Didn't mark a test as done because the duplication had not been eliminated.
- Worked forwards instead of backwards to realize the implementation
- Wrote a test to force the creation of an object we expected to need later
- Started implementing faster 
- Implemented code with casts in one place, then moved the code where it belonged once the test were running.
- Introduced polymorphism to eliminate explicit class checking

Create a real object for the key. I'm not going to write tests for these, because we are writing this code in the context of refactoring. If we get to the payoff of the refactoring and all the tests run, we expect the code to have been exercised. If I was pair programming or if the logic became a bit complex, I would begin writing separate tests. 

Since this is a surprise, let's write a test to communicate what we discovered. 

Several significant techniques:
- Added a parameter, in seconds that we expected we would need
- Factored out the data duplication between code and tests.
- Wrote a test (test array equals) to check an assumption about the operation of Ruby.
- Introduced a private helper class without distinct tests of its own.
- Made a mistake in a refactoring and chose to forge ahead, writing another test to isolate the problem.

Reducing Data Duplication

amount = 5 * 2
amount = amount * 2
amount = amount * multiplier
amount *= multiplier

true
5 == 5
amount == 5
amount == dollar.amount


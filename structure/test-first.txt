Test First

When should you write your tests? Before you write the code that is to be tested. You won't test after. Your goal as a programmer is running functionality. However, you need a way to think about design, you need a method for scope control. 

The more stress you feel, the less likely you are to test enough. When you know you haven't tested enough, you add to your stress. The way to break this loop is to test first. 

When we test first, we reduce the stress, which makes us more likely to test. So the tests must live in virtuous cycles or they will be abondoned when stress increases enough. However, the immediate payoff for automated testing is a design and scope control tool. This suggests that we will be able to start doing it, and keep doing it even under moderate stress.

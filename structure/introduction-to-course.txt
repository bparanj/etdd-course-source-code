You must have already installed Ruby 2.0 or above on your machine. You should also have your favorite text editor such or Sublime, Textmate, RubyMine or something else installed.

In the ‘Let’s Start with the Basics’ section you will learn about the basic terminology to comprehend the material in this course. You will also get a brief introduction to Test Driven Development.

In the ‘Problem Solving Skills’ section you will learn about Problem Domain Analysis and Solution Domain Analysis. 

In the ‘Basics of Test Driven Development’ you will learn how to design test cases and how to write test first.

In the ‘Getting it Right’ section, you will learn about the common mistakes even programmers with many years of TDD experience make, so that you can avoid them.

We will work through the Katas in Section 6, 7 and 8 to learn more about TDD.

In the ‘Techniques in Test Driven Development’ section, you will learn about :
Fake it till you make it
Obvious Implementation
Triangulation
and Transformation Priority Premise

In the final section, we will wrap up the course by looking at the ‘Final TDD Diagram’ that puts all the concepts we learned in the course together. I will also share my key insights and provide resources for learning more.

I will provide links to additional reading for learning more about the concepts covered in some of the lessons.
Most of the sections will also have a quiz at the end of each section to test your knowledge on the topic.
You will have an exercise for almost all lessons. If you grasp the material in the lesson, you will be able to easily complete the exercise on your own without any help. I have created a TDD Course Tools Checklist than you can download. It helps you to practice the concepts you learned in this course. 

At the end of my course, you will be able to:

1. Apply basic problem solving skills to solve problems
2. Apply techniques to do Test Driven Development well
3. Improve the design incrementally
4. Avoid common mistakes while doing TDD
5. Apply TDD to some common interview questions

You will also understand the thought process and steps involved in a test-driven session.

At the end of the first section, you will find Google self assessment form. Please fill out this form to see where you are before we get into this course. We’ll review this document at the end of the course to see how far you have come.

http://goo.gl/forms/HG7nxHsPe7




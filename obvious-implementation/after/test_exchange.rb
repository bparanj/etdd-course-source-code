require 'minitest/autorun'

class Exchanger
  attr_reader :a, :b
  
  def initialize(a, b)
    @a = a
    @b = b
  end
  
  def exchange
    @a, @b = @b, @a
  end  
  
end

class TestExchanger < Minitest::Test
  
  def test_before_exchange
    exchanger = Exchanger.new(1,2)
    
    assert 1, exchanger.a
    assert 2, exchanger.b    
  end
  
  def test_exchange
    exchanger = Exchanger.new(1,2)
    
    exchanger.exchange
    
    assert 2, exchanger.a
    assert 1, exchanger.b
  end
end
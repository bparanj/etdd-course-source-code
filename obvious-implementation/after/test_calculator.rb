require 'minitest/autorun'

class Calculator
  def add(x, y)
    x + y
  end
  
  def subtract(x, y)
    x - y
  end
  
  def multiply(x, y)
    x * y
  end
  
  def divide(x, y)
    x / y
  end
end

class TestCalculator < Minitest::Test
  
  def test_addition
    calculator = Calculator.new
    
    result = calculator.add(3, 2)
    
    assert_equal 5, result
  end
  
  def test_subtraction
    calculator = Calculator.new
    
    result = calculator.subtract(3, 2)
    
    assert_equal 1, result    
  end
  
  def test_multiplication
    calculator = Calculator.new
    
    result = calculator.multiply(2,3)
    
    assert_equal 6, result
  end
  
  def test_division
    calculator = Calculator.new
    
    result = calculator.divide(4,2)
    
    assert_equal 2, result
  end
  
  def test_division_throws_exception_when_divisor_is_zero
    calculator = Calculator.new
    
    assert_raises(ZeroDivisionError) do
      calculator.divide(4,0)
    end
  end
end
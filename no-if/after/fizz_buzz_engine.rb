class FizzBuzzEngine
  
  def initialize(n)
    @n = n
  end
  
  def value
    multiple_of(3) && multiple_of(5) && (return 'FizzBuzz')
    multiple_of(3) && (return 'Fizz')
    multiple_of(5) && (return 'Buzz')
    @n
  end
  
  private
        
  def multiple_of(x)
    @n.modulo(x).zero?
  end
end